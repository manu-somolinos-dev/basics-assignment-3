import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  displayDetails = false;

  clickEvents = [];

  constructor() { }

  ngOnInit() {
  }

  onButtonClick() {
    this.displayDetails = !this.displayDetails;
    this.clickEvents.push('Button clicked at: ' + new Date().toISOString());
  }

  getDisplayValue() {
    return (this.displayDetails) ? 'block' : 'none';
  }
}
